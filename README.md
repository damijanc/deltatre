# Project overview #

Purpose of this project is to read JSON config and use it to build UI "dynamically". Requirements are that config must be refreshed immediately after the file change.

## Design choices ##

1. I have implemented auto load using FileSystemWatcher. This will work well as long as you are not loading config from a network mounted disk. This approach will have issues with it.

1. For JSON serialization I decided  to use strongly typed  DataContractJsonSerializer. I guess the better choice would be to use JSON.NET but I wanted to avoid external dependencies for this demo.

1. Using strongly typed implementation means that I have locked myself to the structure in ```config.json``` file. 

1. As frontend I use ASP.API since the original idea was to use JS client (Angular, Backbone etc.) to build UI but i decided to use RAZOR instead due to time constraints and lack of experience.

1. If I have time I will implement both.

1. Api is reachable on url: ```/api/widgets``` while the RAZOR implementation is reachable on ```/```

1. This demo was build with VISUAL STUDIO 2013 Community Edition. 

### How do I get set up? ###

* Modify ```Web.config/appSettings``` to point to correct file and folder

### What is left to do ###

* cleanup the API project from access static assets
* there are no unit test implemented yet
* error handling is missing or very basic and it should be improved
* UI has basically no design and needs work
* Implement UI in modern JS framework