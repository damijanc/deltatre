﻿using Deltatre.Core.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Deltatre.Core;

namespace Deltatre.Api.Controllers
{
    public class WidgetsController : ApiController
    {


        // GET api/widgets
        public JsonParameters Get()
        {

            JsonParameterReader instance = JsonParameterReader.GetInstance(WebSettingsReader.file, WebSettingsReader.directory);

            return instance.Parameters;
           
        }

        //GET api/widgets/widget
        public string[] Get(string widget)
        {
            return new string[]
            {
                "You just received a widget",
                widget
            };
        }

        // POST api/widgets
        public void Post([FromBody]string value)
        {
        }

        // PUT api/widgets/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/widgets/5
        public void Delete(int id)
        {
        }
    }
}
