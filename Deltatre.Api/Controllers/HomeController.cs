﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Deltatre.Core;

namespace Deltatre.Api.Controllers
{
    public class HomeController : Controller
    {

        public ActionResult Index()
        {

            JsonParameterReader instance = JsonParameterReader.GetInstance(WebSettingsReader.file, WebSettingsReader.directory);

            return View(instance.Parameters);
        }
    }
}
