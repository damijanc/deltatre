﻿using Deltatre.Core.Parameters;
using System.IO;
using System.Runtime.Serialization.Json;

namespace Deltatre.Core
{
    public class JsonReader
    {

        public JsonParameters ReadJson(string path)
        {

            JsonParameters parameters;

            using (FileStream fsSource = new FileStream(path,
            FileMode.Open, FileAccess.Read))
            {

                DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(JsonParameters));
                parameters = (JsonParameters)jsonSerializer.ReadObject(fsSource);
            }

            return parameters;
        }
    }
}
