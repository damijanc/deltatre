﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Deltatre.Core.Parameters
{
    [DataContract]
    public class JsonParameters
    {
        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name = "source")]
        public string Source { get; set; }

        [DataMember(Name = "key")]
        public string Key { get; set; }

        [DataMember(Name = "type")]
        public string Type { get; set; }

        [DataMember(Name = "items")]
        public List<Widget> Widgets;

    }

    [DataContract]
    public class Widget
    {

        [DataMember(Name = "widget-name")]
        public string widgetName { get; set; }

        [DataMember(Name = "widget-code")]
        public string widgetCode { get; set; }

        [DataMember(Name = "inherits")]
        public string Inherits { get; set; }

        [DataMember(Name = "custom-fields")]
        public List<CustomField> customFields { get; set; }


    }

    [DataContract]
    public class CustomField
    {

        [DataMember(Name = "key")]
        public string Key { get; set; }

        [DataMember(Name = "type")]
        public string Type { get; set; }

        [DataMember(Name = "freetext")]
        public Freetext FreeText { get; set; }

        [DataMember(Name = "mandatory")]
        public Boolean Madatory { get; set; }

        [DataMember(Name = "select")]
        public Select Select { get; set; }

        [DataMember(Name = "search")]
        public Search Search { get; set; }
    }

    [DataContract]
    public class Search
    {

        [DataMember(Name = "search-box-label")]
        public string searchBoxLabel { get; set; }

        [DataMember(Name = "search-box-style")]
        public string searchBoxStyle { get; set; }

        [DataMember(Name = "search-button-label")]
        public string searchButtonLabel { get; set; }

        [DataMember(Name = "results-style")]
        public string resultsStyle { get; set; }

        [DataMember(Name = "height")]
        public int Height { get; set; }

        [DataMember(Name = "min-select")]
        public int minSelect { get; set; }


        [DataMember(Name = "max-select")]
        public int maxSelect { get; set; }

        [DataMember(Name = "query-url")]
        public string queryUrl { get; set; }

    }

    [DataContract]
    public class SelectExternal
    {
        [DataMember(Name = "label")]
        public string label { get; set; }

        [DataMember(Name = "type")]
        public string Type { get; set; }

        [DataMember(Name = "items-url")]
        public string itemsUrl { get; set; }
    }

    [DataContract]
    public class Select
    {
        [DataMember(Name = "label")]
        public string label { get; set; }

        [DataMember(Name = "type")]
        public string Type { get; set; }

        [DataMember(Name = "items")]
        public List<SelectItem> selectItems { get; set; }
    }

    [DataContract]
    public class Content
    {
        [DataMember(Name = "image")]
        public string image { get; set; }

        [DataMember(Name = "altText")]
        public string altText { get; set; }

        [DataMember(Name = "trackingPixel")]
        public string trackingPixel { get; set; }
    }

    [DataContract]
    public class SelectItem
    {
        [DataMember(Name = "label")]
        public string Label { get; set; }

        [DataMember(Name = "type")]
        public string Type { get; set; }

        [DataMember(Name = "content")]
        public Content Content { get; set; }
    }

    [DataContract]
    public class Freetext
    {
        [DataMember(Name = "label")]
        public string Label { get; set; }
    }
}
