﻿using System;
using System.Configuration;
using System.Globalization;

namespace Deltatre.Core
{
    public static class WebSettingsReader
    {

        public static string file
        {
            get
            {
                return Setting<string>("file");
            }
        }

        public static string directory
        {
            get
            {
                return Setting<string>("directory");
            }
        }

        private static T Setting<T>(string name)
        {
            string value = ConfigurationManager.AppSettings[name];

            if (value == null)
            {
                throw new Exception(String.Format("Could not find setting '{0}',", name));
            }

            return (T)Convert.ChangeType(value, typeof(T), CultureInfo.InvariantCulture);
        }
    }
}
