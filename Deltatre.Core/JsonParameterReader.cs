﻿using Deltatre.Core.Parameters;
using System;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization.Json;
using System.Security.Permissions;
using System.Threading;

namespace Deltatre.Core
{

    /// <summary>
    /// This is a self refreshing implementation of Json config reader
    /// </summary> 
    public class JsonParameterReader //: IConfigSet
    {

        private JsonParameters parameters;

        public JsonParameters Parameters
        {
            get { return parameters; }
            set { parameters = value; }
        }

        private static JsonParameterReader instance;

        private static object classLockHandle = new object();


        // Instances should only be obtainable using the GetInstance(...) 
        // methods.
        private JsonParameterReader()
        {
        }

        /// <summary>
        /// Configuration file changed event handler.
        /// </summary>
        public event EventHandler<EventArgs> ParametersChanged;

        /// <summary>
        /// Gets a value indicating whether config set will be automatically refreshed 
        /// after json file has changed.
        /// </summary>
        public bool IsAutoRefreshing { get; private set; }


        /// <summary>
        /// Json file changed event handler.
        /// </summary>
        /// <param name="source">Source of event.</param>
        /// <param name="e">Args of event.</param>
        private void OnChanged(object source, FileSystemEventArgs e)
        {

            // wait for file lock to be released
            for (int i = 0; i < 20; i++)
            {
                try
                {
                    JsonParameterReader newInstance = new JsonParameterReader();
                    newInstance.ReadJson(e.FullPath);
                    instance = newInstance;
                    if (null != this.ParametersChanged)
                    {
                        this.ParametersChanged(this, EventArgs.Empty);
                    }

                    return;
                }
                catch (IOException)
                {
                    Thread.Sleep(300);
                }
            }
        }

        /// <summary>
        /// Read the Json configuration
        /// </summary>
        /// <param name="path"></param>
        /// <returns>instance of JsonParameters</returns>
        private JsonParameters ReadJson(string path)
        {

            using (FileStream fsSource = new FileStream(path,
            FileMode.Open, FileAccess.Read))
            {

                DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(JsonParameters));
                this.parameters = (JsonParameters)jsonSerializer.ReadObject(fsSource);
            }

            return this.parameters;
        }

        /// <summary>
        /// Tries to read an json file with path 
        /// <c>directory+"/"+fileName +".json".</c> 
        /// In case it is not successful an exception is thrown.
        /// Returns instance that will be automatically refreshed after save of json file.
        /// </summary>
        /// <param name="fileName">The name of the json file.</param>
        /// <param name="directory">Directory where the json file resides.</param>
        /// <returns>instance.</returns>
        [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
        public static JsonParameterReader GetInstance(
            string fileName, string directory)
        {
            JsonParameterReader configSet = GetInstanceInternal(fileName, directory);
            configSet.SetUpWatcher(fileName, directory);
            return configSet;
        }

        /// <summary>
        /// Tries to read an json file with path 
        /// <c>directory+"/"+fileName +".json".</c> 
        /// In case it is not successful an exception is thrown.
        /// </summary>
        /// <param name="fileName">The name of the json file. </param>
        /// <param name="directory">Directory where the json file resides. 
        /// </param>
        /// <returns>ConfigSet instance.</returns>
        protected static JsonParameterReader GetInstanceInternal(string fileName, string directory)
        {
            // Internally this method is called by GetInstance(string fileName) 
            // with directory == null.
            // If a directory is specifed use the full path as key
            string key = directory == null ?
                                    fileName
                                    :
                                    Path.Combine(directory, fileName);

            if (null == instance)
            {

                if (directory == null)
                    directory = GetConfigDir(fileName);

                string path = Path.Combine(directory, fileName);
                instance = new JsonParameterReader();
                instance.ReadJson(path);
            }
            return instance;
        }

        private void SetUpWatcher(string fileName, string directory)
        {
            // Create a new FileSystemWatcher and set its properties.
            if (!IsAutoRefreshing)
            {
                FileSystemWatcher watcher = new FileSystemWatcher();

                watcher.Path = directory;
                watcher.NotifyFilter = NotifyFilters.LastWrite;
                watcher.Filter = fileName;

                // Add event handlers.
                watcher.Changed += new FileSystemEventHandler(OnChanged);

                // Begin watching.
                watcher.EnableRaisingEvents = true;

            }
        }

        // Returns the directory from where the specified json file should 
        // be searched
        private static string GetConfigDir(string fileName)
        {
            // check if the json file is in the directory of the dll   
            string dllPath = Assembly.GetExecutingAssembly().Location;
            string dllDirectory = Path.GetDirectoryName(dllPath);
            string iniFile = Path.Combine(dllDirectory, fileName);
            if (File.Exists(iniFile))
                return dllDirectory;

            // default value
            string iniFileDirectory = WebSettingsReader.directory;

            return iniFileDirectory;
        }
    }

}
