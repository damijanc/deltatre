﻿using Deltatre.Core.Parameters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;

namespace Deltatre.Core
{
    public class JsonSerializer
    {

        public static string RetrieveJsonData(string file, string directory) {

            JsonParameterReader instance = JsonParameterReader.GetInstance(file, directory);
            JsonParameters parameters = instance.Parameters;

            DataContractJsonSerializer js = new DataContractJsonSerializer(typeof(JsonParameters));
            
            MemoryStream ms = new MemoryStream();
            js.WriteObject(ms, parameters);
            ms.Position = 0;
            StreamReader sr = new StreamReader(ms);
            return sr.ReadToEnd();

        }

    }
}
